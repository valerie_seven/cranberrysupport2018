Étapes:
1. Création des packages afin de séparer les classes dans leur packages respective. Ceci est nécessaire puisque certaine classe n'ont pas à être testé. 
2. Création des tests pour la classe Utilisateur.
	- Retrait des attributs inutiliser (Bureau, mail et téléphone).
	- Instanciation de la liste info dans constructeurs.
	- Ajout du test fetchInfo pour petit constructeurs aussi
	- Enlever le Override des getRole
3. Création des tests pour la classe Technicien.
	- Ajout getter pour la liste en cours de requête et pour les requêtes finies.
	- Instanciation des listes dans le petit constructeur.
4. Création des tests pour la classe Client.
5. Création des tests pour la classe Commentaire.
	- Changer le droit accès au constructeur a public.
6. Création des tests pour la classe Requête.
	- Retrait petit constructeurs inutile et variable tempo.
	- Ajout du code nécessaire pour faire fonctionner uploadFile
7. Création des tests de la classe BanqueRequetes.
	- Getter de la liste de requête.
