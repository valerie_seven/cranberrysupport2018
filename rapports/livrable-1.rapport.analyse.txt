Problème: Il existe aucun test pour tout le système. Rien ne prouve que les fonctionnalités fonctionnent comme elles le devraient. À chaque fois il faut tester à la main dans un main ou espérer que ça fonctionne.
Solution: Ajout de test unitaire, test de connexion avec la base de données et de test d'intégration.

Problème: Les classes sont présentement tous dans le même package. On ne sait pas quelle classe sert à quoi dans le système.
Solution: Création de package séparant classes pour les vues, les classe pour les fonctionnalités, les classes servant pour les modèle et les classe servant de contrôleurs.

Problème: Plusieurs méthode font plusieurs choses et/ou sont trop longue.
Solution: Extraction en plusieurs méthodes afin de pouvoir bien diviser les longues méthodes en recette plus lisible. La création de test pour chaque méthode permet aussi de bien tester le code.

Problème: Il y a beaucoup d'utilisation de magic numbers (de caractère spécial aussi).
Solution: Création de constante avec des nom significatif afin de mieux lire le code.

Problème: Les noms de classe, méthodes et variables sont à la fois en anglais et en français ce qui est parfois plus difficile à lire, car il faut constamment faire la traduction dans notre tête.
Solution: Renommer le tout seulement en anglais.

Problème: Il y a beaucoup de commentaire vide ou inutile.
Solution: Enlever tous les commentaires et renommer les noms de classe, méthodes et variables afin d'être plus significatif.

Problème: Il y a beaucoup de séparation de lignes vide. Les lignes vide sont utiles lorsque l'on veut séparer un bloc d'instructions à un autre.
Solution: Retirer toute les lignes vide qui n'ont pas lui d'être.

Problème: Il y a beaucoup d'indentations qui ne respecte pas le bloc d'instructions. Effectivement, les instructions appartenant au même bloc doivent avoir la même indentation.
Solution: Enlever/ajouter les indentations au endroits nécessaire.

Problème: Il y a des if/else utilisé de manière incorrecte. Parfois une else inutile, parfois un if/else pour définir quoi faire selon un type.
Solution: Retrait des condition inutile et l'ajout d'héritage et de variable pour mieux contrôler lorsqu'il y a des opérations à effectuer de plus dans des conditions particulières.

Problème: Il y a des variables avec des noms non significatif (a, b, c, d). On ne sait pas à quoi elles servent exactement et on doit lire le code pour le découvrir.
Solution: Renommer les variables avec des nom significatif.

Problème: Il y a du code dupliquer. 
Solution: Extraction de méthode afin de pouvoir réduire le nombre de risque en fessant du copy paste partout. On fait ainsi le changement a une seule place.

Problème: Certaine configuration d'objet est extrêmement longue ! parfois sur une vingtaine de ligne. Ça rend la lecture du code difficile et le taux de "WTF" est très élevé.
Solution: Création d'objet avec des constructeurs pour mieux comprendre qui est quoi et pourquoi ils sont configurés ainsi. Le design pattern Builder est une technique à considérer ici.

Problème: Une classe contient plusieurs listes pour chaque type d'état de requête. Utilisation de ressource non nécessaire.
Solution: Ajout d’une variable décrivant un état ou l'utilisation d'héritage afin d'utiliser le polymorphisme.
