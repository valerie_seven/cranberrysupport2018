package model;

import util.Character;

public class Comment {

	private String comment;
	private User author;

	public Comment(String comment, User author) {
		this.comment = comment;
		this.author = author;
	}

	public User getAuthor() {
		return author;
	}

	public String getComment() {
		return comment;
	}

	public String toString() {
		return getAuthor().getUsername() + Character.COLON + getComment() + Character.NEW_LINE;
	}
}