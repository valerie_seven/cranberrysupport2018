package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import service.RequestManager;
import service.UserManager;

public class Request {
	public enum Statut {
		OPEN("ouvert"), 
		CURRENT("en traitement"), 
		ABANDON("Abandon"), 
		SUCCESS("Succès");

		private final String text;

		Statut(final String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public static Statut fromString(String text) {
			if (text != null) {
				for (Statut statut : Statut.values()) {
					if (statut.text.equals(text))
						return statut;
				}
			}
			return null;
		}
	}
	public enum Category {
		WORKPLACE("Poste de travail"), 
		SERVER("Serveur"), 
		WEB_SERVICE("Service web"), 
		USER_ACCOUNT("Compte usager"),
		OTHER("Autre");

		private final String text;

		Category(final String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public static Category fromString(String text) {
			if (text != null) {
				for (Category category : Category.values()) {
					if (category.text.equals(text))
						return category;
				}
			}
			return null;
		}
	}

	private String subject;
	private String description;
	private File file;
	private Integer number;
	private User client;
	private Technician technician;
	private Statut statut;
	private Category category;
	private ArrayList<Comment> comments;
	
	public Request(String subject, String description, User client, Category category)
	    throws FileNotFoundException, FileNotFoundException, IOException {
		this.subject = subject;
		this.description = description;
		this.number = RequestManager.getInstance().incrementeNo();
		this.client = client;

		if (client instanceof Technician) {
			this.technician = (Technician) client;
		}

		this.statut = Statut.OPEN;
		this.category = category;
		this.comments = new ArrayList<Comment>();
	}
	
	public Request(String subject, String description, File file, User client, Technician technician, Statut statut, 
			Category category) throws FileNotFoundException, IOException {
		this.subject = subject;
		this.description = description;
		this.file = file;
		this.number = RequestManager.getInstance().incrementeNo();
		this.client = client;

		if (client instanceof Technician) {
			this.technician = (Technician) client;
		}

		this.statut = statut;
		this.category = category;
		this.comments = new ArrayList<Comment>();
	}
	
	public static class Builder {
		private String subject;
		private String description;
		private File file;
		private User client;
		private Technician technician;
		private Statut statut;
		private Category category;
		
		public Builder() { }
		
		public Builder setSubject(String subject) {
			this.subject = subject;
			return this;
		}
		
		public Builder setDescription(String description) {
			this.description = description;
			return this;
		}

		public Builder setFile(String filePath) {
			this.file = new File(filePath);
			return this;
		}

		public Builder setClient(String username) throws FileNotFoundException {
			this.client = UserManager.getInstance().FindUserByUsername(username);
			return this;
		}

		public Builder setTechnician(String username) throws FileNotFoundException {
			this.technician = (Technician) UserManager.getInstance().FindUserByUsername(username);
			return this;
		}
		
		public Builder setStatut(String statut) {
			this.statut = Request.Statut.fromString(statut);
			return this;
		}

		public Builder setCategory(String category) {
			this.category = Request.Category.fromString(category);
			return this;
		}
		
		public Request build() throws FileNotFoundException, IOException {
			return new Request(subject, description, file, client, technician, statut, category);
		}

	};

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return this.description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public File getFile() {
		return this.file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public Integer getNumber() {
		return this.number;
	}

	public User getClient() {
		return this.client;
	}

	public Technician getTechnician() {
		return this.technician;
	}

	public void setTechnician(Technician technician) {
		if (technician != null) {
				this.technician = technician;
				this.technician.addRequest(this);
		}
	}

	public Statut getStatut() {
		return this.statut;
	}

	public void setStatut(Statut statut) {
		this.statut = statut;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public ArrayList<Comment> getComments() {
		return this.comments;
	}

	public void uploadFile(String filepath) {
		//TODO
	}

	public void addComment(Comment comment) {
		comments.add(comment);
	}

}