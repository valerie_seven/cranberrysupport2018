package model;

import java.util.ArrayList;

public abstract class User {
	protected String lastname;
	protected String firstname;
	protected String username;
	protected String password;
	protected String role;
	protected ArrayList<Request> requests;
	protected ArrayList<String> informations;

	public User(String firstname, String lastname, String username, String password, String role) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.username = username;
		this.password = password;
		this.role = role;
		this.requests = new ArrayList<>();
		this.informations = new ArrayList<>();
	}
	
	public static User create(String firstname, String lastname, String username, String password,String role) {
		User user = null;
		
		if(role.equals("client")) {
			user = new Client(firstname, lastname, username, password, role);
		} else if(role.equals("technicien")) {
			user = new Technician(firstname, lastname, username, password, role);
		}
		
		return user;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public String getUsername() {
		return username;
	}

	public String getRole() {
		return this.role;
	}

	public ArrayList<String> getInformations() {
		this.informations.add(lastname);
		this.informations.add(firstname);
		this.informations.add(username);
		this.informations.add(role);
		
		return this.informations;
	}

	public boolean isCredentialValid(String username, String password) {
		return this.username.equalsIgnoreCase(username) && this.password.equals(password);
	}

	public ArrayList<Request> getRequests(Request.Statut statut){
		ArrayList<Request> requests = new ArrayList<Request>();
		
		for (Request request : this.requests) {
			if(request.getStatut().equals(statut)) {
				requests.add(request);
			}
		}
    
    return requests;
	}
	
	public ArrayList<Request> getRequests(){
		return this.requests;
	}
	
	public void addRequest(Request request) {
		this.requests.add(request);
	}
}