package model;

import java.util.ArrayList;
import util.Character;

public class Technician extends User {
	public static final String STATUT = "Statut ";

	public Technician(String firstname, String lastname, String username, String password, String role) {
		super(firstname, lastname, username, password, role);
	}

	public ArrayList<Request> getPersonnalRequests() {
		ArrayList<Request> personnalRequests = new ArrayList<>();
		
		for (Request request : this.requests) {
			if(request.getClient().equals(this)) {
				personnalRequests.add(request);
			}
		}
		
		return personnalRequests;
	}

	public String getNumberOfRequestPerStatut() {
		String result = "";
		
		for (Request.Statut statut : Request.Statut.values()) {
			result += formatNumberOfRequests(statut);
		}

		return result;
	}
	
	private String formatNumberOfRequests(Request.Statut statut) {
		int numberOfRequests = getNumberOfRequests(statut);
		return STATUT + statut.toString() + Character.COLON + numberOfRequests + Character.NEW_LINE;
	}
	
	private int getNumberOfRequests(Request.Statut statut) {
		return getRequests(statut).size();
	}
}