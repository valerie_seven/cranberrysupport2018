package service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

import model.User;

public class UserManager {
	private static final String SPLITTER_USER = ";";
	private static final int USERS_MINIMUM = 100;
	private static final int DATA_FIRSTNAME = 0;
	private static final int DATA_LASTNAME = 1;
	private static final int DATA_USENAME = 2;
	private static final int DATA_PASSWORD = 3;
	private static final int DATA_ROLE = 4;
	
	private static UserManager instance = null;
	private ArrayList<User> users;

	private UserManager() {
		this.users = new ArrayList<User>(USERS_MINIMUM);
	}

	public static UserManager getInstance() throws FileNotFoundException {
		if (instance == null) {
			instance = new UserManager();
			instance.loadUsers();
		}

		return instance;
	}

	public void loadUsers() throws FileNotFoundException {
		Scanner scanner = new Scanner(new FileReader("dat/BanqueUtilisateur.txt"));
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			String[] data = line.split(SPLITTER_USER);
			this.addUser(data[DATA_FIRSTNAME], data[DATA_LASTNAME], data[DATA_USENAME], data[DATA_PASSWORD], data[DATA_ROLE]);
		}
		scanner.close();
	}

	public ArrayList<User> getUsers(String role) {
		ArrayList<User> users = new ArrayList<User>(100);
		
		for (User user : this.users) {
			if(user.getRole().equals(role)) {
				users.add(user);
			}
		}
		
		return users;
	}

	public User findUser(User user) {
		if (this.users.contains(user)) {
			return user;
		}
		
		return null;
	}

	public User FindUserByUsername(String username) {
		for (User user : this.users) {
			if(user.getUsername().equals(username)) {
				return user;
			}
		}
		
		return null;
	}

	public void addUser(String firstname, String lastname, String username, String password, String role) {
		User user = User.create(firstname, lastname, username, password, role);
		
		if(user != null) {
			this.users.add(user);
		}
	}
}