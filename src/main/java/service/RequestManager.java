package service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import model.Comment;
import model.Request;
import model.Technician;
import model.User;
import util.Character;

public class RequestManager {

	public static final String DAT_BANQUE_REQUETES_TXT = "dat/BanqueRequetes.txt";
	
	private static final String SPLITTER_REQUEST = ";~";
	private static final String SPLITTER_COMMENT_USERNAME = "&#";
	private static final String SPLITTER_COMMENT = "/%";
	
	private static final int DATA_COMMENTS = 7;
	private static final int DATA_TECHNICIAN = 6;
	private static final int DATA_FILE_PATH = 5;
	private static final int DATA_STATUT = 4;
	private static final int DATA_CATEGORY = 3;
	private static final int DATA_CLIENT = 2;
	private static final int DATA_DESCRIPTION = 1;
	private static final int DATA_SUBJECT = 0;
	
	private static final int REQUESTS_MINIMUM = 100;
	
	private static RequestManager instance = null;
	private Integer number = 0;
	private ArrayList<Request> requests;

	private RequestManager() throws FileNotFoundException, IOException {
		this.requests = new ArrayList<Request>(REQUESTS_MINIMUM);
	}

	public static RequestManager getInstance() throws FileNotFoundException, IOException {
		if (instance == null) {
			instance = new RequestManager();
			instance.loadRequest();
		}
		return instance;
	}

	public void addRequest(String subject, String description, User client, Request.Category category)
	    throws FileNotFoundException, IOException {
		addRequest(new Request(subject, description, client, category));
	}

	public ArrayList<Request> getRequests(Request.Statut statut) {
		ArrayList<Request> requests = new ArrayList<Request>();
		
		for (Request request : this.requests) {
			if(request.getStatut().equals(statut)) {
				requests.add(request);
			}
		}
		
		return requests;
	}

	public ArrayList<Request> getRequests() {
		return this.requests;
	}

	public void assignTechnicianToRequest(Technician technician, Request request) {
		request.setStatut(Request.Statut.CURRENT);
		request.setTechnician(technician);
	}

	public Request getLastRequest() {
		return this.requests.get(requests.size() - 1);
	}

	public Integer incrementeNo() {
		return number++;
	}

	private void loadRequest() throws FileNotFoundException, IOException {
		File file = new File(DAT_BANQUE_REQUETES_TXT);
		if (file.exists()) {
			Scanner scanner = new Scanner(new FileReader(file));
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] data = line.split(SPLITTER_REQUEST, -1);
				this.addRequestFromString(data[DATA_SUBJECT], data[DATA_DESCRIPTION], data[DATA_CLIENT], data[DATA_CATEGORY], 
						data[DATA_STATUT], data[DATA_FILE_PATH], data[DATA_TECHNICIAN], data[DATA_COMMENTS]);
			}
			scanner.close();
		}
	}

	private void addRequestFromString(String subject, String description, String clientUsername, String category, String statut,
	    String filePath, String technicianUsername, String comments) throws FileNotFoundException, IOException {
		Request request = new Request.Builder()
				.setSubject(subject)
				.setDescription(description)
				.setFile(filePath)
				.setClient(clientUsername)
				.setTechnician(technicianUsername)
				.setStatut(statut)
				.setCategory(category)
				.build();
		
		for (Comment comment : getComments(comments)) {
			request.addComment(comment);
		}
		
		addRequest(request);
	}
	
	private void addRequest(Request request) {
		requests.add(request);
		request.getClient().addRequest(request);
	}
	
	private ArrayList<Comment> getComments(String text) throws FileNotFoundException{
		ArrayList<Comment> comments = new ArrayList<>();
		if (!text.equals("")) {
			 String[] data = text.split(SPLITTER_COMMENT);
					 for (String comment : data) {
						 String[] commentParts = comment.split(SPLITTER_COMMENT_USERNAME);
						 User user = UserManager.getInstance().FindUserByUsername(commentParts[1]);
						 comments.add(new Comment(commentParts[0], user));
					}
		}
		return comments;
	}
	
	public void saveRequests() throws IOException {
		FileWriter fileWriter = new FileWriter(DAT_BANQUE_REQUETES_TXT);
		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
		
		for (Request request : requests) {
			bufferedWriter.write(getRequestToString(request));
		}
		
		bufferedWriter.close();
	}
	
	private String getRequestToString(Request request) {
		return request.getSubject() + SPLITTER_REQUEST
				 + request.getDescription() + SPLITTER_REQUEST
				 + request.getClient().getUsername() + SPLITTER_REQUEST
				 + request.getCategory().toString() + SPLITTER_REQUEST
				 + request.getStatut().toString() + SPLITTER_REQUEST
				 + getFilePathToString(request) + SPLITTER_REQUEST
				 + getTechnicianToString(request) + SPLITTER_REQUEST
				 + getCommentsToString(request) + Character.NEW_LINE;
	}

	private String getFilePathToString(Request request) {
		String filePath = "";
		if(request.getFile() != null) {
			filePath = request.getFile().getPath();
		}
		return filePath;
	}

	private String getTechnicianToString(Request request) {
		String technician = "";
		if(request.getTechnician() != null) {
			technician = request.getTechnician().getUsername();
		}
		return technician;
	}
	
	private String getCommentsToString(Request request) {
		String comments = "";
		for (Comment comment : request.getComments()) {
			comments += comment.getComment() + SPLITTER_COMMENT_USERNAME
									+ comment.getAuthor().getUsername();
			if(request.getComments().indexOf(comment) != request.getComments().size()) {
				comments += SPLITTER_COMMENT;
			}
		}
		return comments;
	}
}
