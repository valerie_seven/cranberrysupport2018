package view;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;

import model.Comment;
import model.Request;
import model.Technician;
import model.User;
import service.RequestManager;
import service.UserManager;

public class TechLoggedFrame extends javax.swing.JFrame {

    Technician him;
    String clientname;
    ArrayList<Request> listTechAssigne;
    ArrayList<Request> listDispo;
    ArrayList<User> listTechniciens;
    ArrayList<Request> listRequeteTech;
    Request aAfficher;
    JFrame prec;
    private File file;
    private boolean showComments = false;
    DefaultListModel d = new DefaultListModel();
    DefaultListModel a = new DefaultListModel();
    DefaultListModel b = new DefaultListModel();
    DefaultListModel c = new DefaultListModel();

    /** Creates new form TechLoggedFrame */
    public TechLoggedFrame(User tech) throws FileNotFoundException, IOException {
        initComponents();
        this.setVisible(true);
        him = (Technician) tech;
        //Toutes les listes utiles
        listTechAssigne = tech.getRequests();
        listDispo = RequestManager.getInstance().getRequests(Request.Statut.OPEN);
        listTechniciens = UserManager.getInstance().getUsers("technicien");
        listRequeteTech = him.getPersonnalRequests();

        //Le code qui suit est l'initialisation de toutes les listes dynamiques
        //de l'interface du Technicien

        //Liste des requetes assignées
        if (listTechAssigne.isEmpty()) {
            a.addElement("Vous n'avez pas de requête encore.");
            reqAssignee.setModel(a);
        } else {
            for (int i = 0; i < listTechAssigne.size(); i++) {
                a.addElement(listTechAssigne.get(i).getSubject());
            }
            reqAssignee.setModel(a);
        }

        //Liste des requetes disponible (ouverte)
        if (listDispo.isEmpty()) {
            b.addElement("Il n'y a pas de requêtes pour le moment.");
            reqDispo.setModel(b);
            prendrereqBtn.setEnabled(false);
        } else {
            for (int i = 0; i < listDispo.size(); i++) {
                b.addElement(listDispo.get(i).getSubject());
            }
            reqDispo.setModel(b);
            prendrereqBtn.setEnabled(true);

        }

        //Liste des techniciens
        if (listTechniciens.isEmpty()) {
            c.addElement("Il n'y a pas de technicien");
            supportList.setModel(c);
        } else {
            for (int i = 0; i < listTechniciens.size(); i++) {
                c.addElement(listTechniciens.get(i).getUsername());
            }
            supportList.setModel(c);

        }

    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fileChooser = new javax.swing.JFileChooser();
        reqAssignedLbl = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        reqAssignee = new javax.swing.JList();
        reqSelLbl = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        requeteArea = new javax.swing.JTextArea();
        comLbl = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        commentsArea = new javax.swing.JTextArea();
        addComLbl = new javax.swing.JLabel();
        comIn = new javax.swing.JTextField();
        catBox = new javax.swing.JComboBox();
        reqDispoLbl = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        reqDispo = new javax.swing.JList();
        prendrereqBtn = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        descReqArea = new javax.swing.JTextArea();
        newRequeteBtn = new javax.swing.JButton();
        assignerReqBtn = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        supportList = new javax.swing.JList();
        finBox = new javax.swing.JComboBox();
        ajoutfichierBtn = new javax.swing.JButton();
        affRapportBtn = new javax.swing.JButton();
        quitBtn = new javax.swing.JButton();
        fileLbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        reqAssignedLbl.setText("Les requêtes qui vous sont assignées:");

        reqAssignee.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                reqAssigneeValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(reqAssignee);

        reqSelLbl.setText("Requête sélectionnée:");

        requeteArea.setColumns(20);
        requeteArea.setRows(5);
        requeteArea.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                requeteAreaPropertyChange(evt);
            }
        });
        jScrollPane2.setViewportView(requeteArea);

        comLbl.setText("Commentaires:");

        commentsArea.setColumns(20);
        commentsArea.setRows(5);
        jScrollPane3.setViewportView(commentsArea);

        addComLbl.setText("Ajouter commentaire:");

        comIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comInActionPerformed(evt);
            }
        });
        comIn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comInKeyPressed(evt);
            }
        });

        catBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Changer la catégorie", "Poste de travail", "Serveur", "Service web", "Compte usager", "Autre" }));
        catBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                catBoxItemStateChanged(evt);
            }
        });
        catBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                catBoxActionPerformed(evt);
            }
        });

        reqDispoLbl.setText("Requêtes disponibles non-assignée:");

        reqDispo.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                reqDispoValueChanged(evt);
            }
        });
        jScrollPane4.setViewportView(reqDispo);

        prendrereqBtn.setText("Prendre la requête sélectionnée");
        prendrereqBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prendrereqBtnActionPerformed(evt);
            }
        });

        descReqArea.setColumns(20);
        descReqArea.setRows(5);
        jScrollPane5.setViewportView(descReqArea);

        newRequeteBtn.setText("Créer une nouvelle requête");
        newRequeteBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newRequeteBtnActionPerformed(evt);
            }
        });

        assignerReqBtn.setText("Assigner la requête à ce membre");
        assignerReqBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignerReqBtnActionPerformed(evt);
            }
        });

        jScrollPane6.setViewportView(supportList);

        finBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Finaliser", "Succès", "Abandon", " " }));
        finBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                finBoxItemStateChanged(evt);
            }
        });
        finBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                finBoxActionPerformed(evt);
            }
        });

        ajoutfichierBtn.setText("Ajouter un fichier");
        ajoutfichierBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ajoutfichierBtnActionPerformed(evt);
            }
        });

        affRapportBtn.setText("Afficher le rapport");
        affRapportBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                affRapportBtnActionPerformed(evt);
            }
        });

        quitBtn.setText("Quitter");
        quitBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitBtnActionPerformed(evt);
            }
        });

        fileLbl.setText("(pas de fichier attaché)");
        fileLbl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                fileLblMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(reqDispoLbl)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(32, 32, 32)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jScrollPane5)
                                    .addComponent(prendrereqBtn))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.LEADING, 0, 0, Short.MAX_VALUE)
                                    .addComponent(assignerReqBtn, javax.swing.GroupLayout.Alignment.LEADING))))
                        .addGap(86, 86, 86))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)
                            .addComponent(reqAssignedLbl, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(reqSelLbl)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(catBox, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(finBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(21, 21, 21)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(comLbl)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(addComLbl)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(comIn)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(ajoutfichierBtn)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(fileLbl)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(quitBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(affRapportBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(newRequeteBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(newRequeteBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(affRapportBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(quitBtn))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(reqAssignedLbl)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 171, Short.MAX_VALUE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(comLbl)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(addComLbl)
                                        .addComponent(comIn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(ajoutfichierBtn)
                                        .addComponent(fileLbl)))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(reqSelLbl)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(catBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(finBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(3, 3, 3)))
                .addGap(38, 38, 38)
                .addComponent(reqDispoLbl)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(prendrereqBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(assignerReqBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //Quitter
    private void quitBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quitBtnActionPerformed
        System.exit(0);
    }//GEN-LAST:event_quitBtnActionPerformed

    private void requeteAreaPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_requeteAreaPropertyChange
        // TODO add your handling code here:
        //a supprimer
    }//GEN-LAST:event_requeteAreaPropertyChange

    //On change la sortie du TextArea en fonction de la requete selectionnée
    private void reqAssigneeValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_reqAssigneeValueChanged
        updateRequeteA();
        updateCommentaires();
    }//GEN-LAST:event_reqAssigneeValueChanged

    //On change la sortie du TextArea en fonction de la requete selectionnée
    private void reqDispoValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_reqDispoValueChanged
        if (!listDispo.isEmpty()) {
            updateRequeteD();
        }
    }//GEN-LAST:event_reqDispoValueChanged

    //Change la catégorie
    private void catBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_catBoxItemStateChanged
        if (catBox.getSelectedIndex() > 0) {
            listTechAssigne.get(reqAssignee.getSelectedIndex()).setCategory(Request.Category.fromString((String) catBox.getSelectedItem()));
            updateRequeteA();
        }
    }//GEN-LAST:event_catBoxItemStateChanged

    //Assigne une requete au technicien à qui appartient cette Frame
    private void prendrereqBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prendrereqBtnActionPerformed
        if (reqDispo.getSelectedIndex() >= 0) {
            Request r = listDispo.get(reqDispo.getSelectedIndex());
            r.setTechnician(him);
            r.setStatut(Request.Statut.CURRENT);


            listTechAssigne = him.getRequests();

            a = new DefaultListModel();
            for (int i = 0; i < listTechAssigne.size(); i++) {
                a.addElement(listTechAssigne.get(i).getSubject());
            }
            reqAssignee.setModel(a);


            b.remove(reqDispo.getSelectedIndex());
            listDispo.remove(r);

            updateTables();
        }

    }//GEN-LAST:event_prendrereqBtnActionPerformed

    private void comInActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comInActionPerformed
        // TODO add your handling code here:.
        // a supprimer
    }//GEN-LAST:event_comInActionPerformed

    //Ajoute le commentaire lorsqu'on appuie sur ENTER
    private void comInKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comInKeyPressed
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            aAfficher.addComment(new Comment(comIn.getText(), him));
            String s = "";
            for (Comment cmt : aAfficher.getComments()) {
                s += cmt.toString();
            }
            commentsArea.setText(s);
            comIn.setText("");
        }
    }//GEN-LAST:event_comInKeyPressed

    //A l'aide du FileChooser, on ajoute ou on change le fichier attaché à la requete
    private void ajoutfichierBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ajoutfichierBtnActionPerformed
        fileChooser.showOpenDialog(prec);

        File f = fileChooser.getSelectedFile();
        if (f != null) {
            try {
                file = new File("dat/" + f.getName());
                InputStream srcFile = new FileInputStream(f);
                OutputStream newFile = new FileOutputStream(file);
                byte[] buf = new byte[4096];
                int len;
                while ((len = srcFile.read(buf)) > 0) {
                    newFile.write(buf, 0, len);
                }
                srcFile.close();
                newFile.close();
            } catch (IOException ex) {
                Logger.getLogger(newRequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (file.exists()) {
                fichierAddOkFrame ok = new fichierAddOkFrame();
                ok.setVisible(true);
            }
        }

        try {
            listTechAssigne.get(reqAssignee.getSelectedIndex()).setFile(file);
        } catch (IndexOutOfBoundsException e) {
        }

        updateRequeteA();
}//GEN-LAST:event_ajoutfichierBtnActionPerformed

    //Permet d'ouvrir le fichier
    private void fileLblMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_fileLblMouseClicked
        try {
            Request req = listTechAssigne.get(reqAssignee.getSelectedIndex());
            //Process p = Runtime.getRuntime().exec(req.getFichier().getAbsolutePath());
            java.awt.Desktop dt = java.awt.Desktop.getDesktop();
            dt.open(req.getFile());
        } catch (IOException ex) {
            Logger.getLogger(TechLoggedFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_fileLblMouseClicked

    private void finBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_finBoxActionPerformed
        // TODO add your handling code here:
        //a supprimer
    }//GEN-LAST:event_finBoxActionPerformed

    private void catBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_catBoxActionPerformed
        // TODO add your handling code here:
        //a supprimer
    }//GEN-LAST:event_catBoxActionPerformed

    //Finalise une requete
    private void finBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_finBoxItemStateChanged
        if (finBox.getSelectedIndex() > 0) {
            listTechAssigne.get(reqAssignee.getSelectedIndex()).setStatut(Request.Statut.fromString((String) finBox.getSelectedItem()));
            updateRequeteA();
        }
    }//GEN-LAST:event_finBoxItemStateChanged

    //Assigne une requete à un autre membre selectionné dans la liste
    private void assignerReqBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assignerReqBtnActionPerformed
        if (reqDispo.getSelectedIndex() >= 0) {
            Request r = listDispo.get(reqDispo.getSelectedIndex());
            Technician autre = (Technician) listTechniciens.get(supportList.getSelectedIndex());
            r.setTechnician(autre);
            r.setStatut(Request.Statut.CURRENT);


            b.remove(reqDispo.getSelectedIndex());
            listDispo.remove(r);

            updateTables();
        }
    }//GEN-LAST:event_assignerReqBtnActionPerformed

    //Créer une nouvelle requête
    private void newRequeteBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newRequeteBtnActionPerformed
        this.setVisible(false);
        newRequeteFrame nouvelleRequete = new newRequeteFrame(him, this);

    }//GEN-LAST:event_newRequeteBtnActionPerformed

    //génere et affiche le rapport
    private void affRapportBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_affRapportBtnActionPerformed

        String rap = "";
        Technician tempo;
        for (int i = 0; i < listTechniciens.size(); i++) {
            rap += listTechniciens.get(i).getUsername() + ":\n";
            tempo = (Technician) listTechniciens.get(i);
            rap += tempo.getNumberOfRequestPerStatut() + "\n";

        }
        rapportFrame rapport = new rapportFrame(rap);
        rapport.setVisible(true);

    }//GEN-LAST:event_affRapportBtnActionPerformed
    private void listRequeteListValueChanged(javax.swing.event.ListSelectionEvent evt) {
        //a supprimer
    }
    /**
     * @param args the command line arguments
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel addComLbl;
    private javax.swing.JButton affRapportBtn;
    private javax.swing.JButton ajoutfichierBtn;
    private javax.swing.JButton assignerReqBtn;
    private javax.swing.JComboBox catBox;
    private javax.swing.JTextField comIn;
    private javax.swing.JLabel comLbl;
    private javax.swing.JTextArea commentsArea;
    private javax.swing.JTextArea descReqArea;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JLabel fileLbl;
    private javax.swing.JComboBox finBox;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JButton newRequeteBtn;
    private javax.swing.JButton prendrereqBtn;
    private javax.swing.JButton quitBtn;
    private javax.swing.JLabel reqAssignedLbl;
    private javax.swing.JList reqAssignee;
    private javax.swing.JList reqDispo;
    private javax.swing.JLabel reqDispoLbl;
    private javax.swing.JLabel reqSelLbl;
    private javax.swing.JTextArea requeteArea;
    private javax.swing.JList supportList;
    // End of variables declaration//GEN-END:variables

    //Les 3 methodes suivantes mettent à jour les composantes dynamiques de la Frame
    private void updateRequeteA() {
        if (reqAssignee.getSelectedIndex() >= 0) {
            aAfficher = listTechAssigne.get(reqAssignee.getSelectedIndex());
            requeteArea.setText("Sujet: " + aAfficher.getSubject()
                    + "\nDescription: " + aAfficher.getDescription()
                    + "\nCatégorie: " + aAfficher.getCategory().toString()
                    + "\nStatut: " + aAfficher.getStatut().toString());
            String s = "";
            for (Comment cmt : aAfficher.getComments()) {
                s += cmt.toString();
            }
            commentsArea.setText(s);
            if (aAfficher.getFile() != null && aAfficher.getFile().exists()) {
                fileLbl.setVisible(true);
                fileLbl.setText("Afficher " + aAfficher.getFile().getPath());
            } else {
                fileLbl.setVisible(false);
            }
        } else {
            requeteArea.setText("");
        }
        if (aAfficher.getStatut() == Request.Statut.CURRENT) {
            finBox.setEnabled(true);
        } else {
            finBox.setEnabled(false);
        }
    }

    private void updateRequeteD() {
        if (reqDispo.getSelectedIndex() >= 0) {
            aAfficher = listDispo.get(reqDispo.getSelectedIndex());
            descReqArea.setText("Sujet: " + aAfficher.getSubject()
                    + "\nDescription: " + aAfficher.getDescription()
                    + "\nCatégorie: " + aAfficher.getCategory().toString()
                    + "\nStatut: " + aAfficher.getStatut().toString());
        } else {
            descReqArea.setText("");
        }
    }

    private void updateCommentaires() {
        if (aAfficher != null) {
            if (!showComments) {
                String s = "";
                for (Comment c : aAfficher.getComments()) {
                    s += c.getAuthor().getUsername() + ": " + c.getComment() + "\n";

                }
                commentsArea.setText(s);
                showComments = true;

            }

        }
    }

    private void updateTables() {
        reqAssignee.setModel(a);
        if (!b.isEmpty()) {
            reqDispo.setModel(b);
            prendrereqBtn.setEnabled(true);
        } else {
            b.addElement("Il n'y a pas de requêtes pour le moment.");
            reqDispo.setModel(b);
            prendrereqBtn.setEnabled(false);
        }
    }
}
