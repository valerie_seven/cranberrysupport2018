package view;


import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.User;
import service.UserManager;


public class ClientFrame extends javax.swing.JFrame {

    /** Creates new form RaspberryFrame */
    public ClientFrame() {
        initComponents();
        setVisible(true);
    }
    private String nomUtil;
    private String motdepasse;
    private User potentiel;
    

   
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        nameLbl = new javax.swing.JLabel();
        mdpLbl = new javax.swing.JLabel();
        exName = new javax.swing.JLabel();
        exMdp = new javax.swing.JLabel();
        utilisateur = new javax.swing.JTextField();
        mdp = new javax.swing.JTextField();
        okBtn = new javax.swing.JButton();
        annulerBtn = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        nameLbl.setText("Nom d'utilisateur:");

        mdpLbl.setText("Mot de passe:");

        exName.setText("ex.: Isabeau Desrochers");

        exMdp.setText("ex.: MonMdp");

        okBtn.setText("OK");
        okBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                okBtnMouseClicked(evt);
            }
        });

        annulerBtn.setText("Annuler");
        annulerBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                annulerBtnMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(okBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(annulerBtn))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nameLbl)
                            .addComponent(mdpLbl))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mdp, javax.swing.GroupLayout.DEFAULT_SIZE, 149, Short.MAX_VALUE)
                            .addComponent(utilisateur, javax.swing.GroupLayout.DEFAULT_SIZE, 149, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(exMdp)
                    .addComponent(exName))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nameLbl)
                    .addComponent(utilisateur, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(exName))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mdpLbl)
                    .addComponent(mdp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(exMdp))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(annulerBtn)
                    .addComponent(okBtn))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //Quand on clique sur OK, on vérifie la validité des informations
    private void okBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_okBtnMouseClicked
        try {
            nomUtil = utilisateur.getText();
            motdepasse = mdp.getText();
            
            potentiel = UserManager.getInstance().FindUserByUsername(nomUtil);
            if(potentiel == null){
                ErrorLoginFrame mauvaisLogin = new ErrorLoginFrame();
                this.setVisible(false);
            } 
            
            if(potentiel.isCredentialValid(nomUtil,motdepasse)){
                this.setVisible(false);
                ClientLoggedFrame fenetreClient = new ClientLoggedFrame(potentiel);
            }
            else{
                ErrorLoginFrame mauvaisLogin = new ErrorLoginFrame();
                this.setVisible(false);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ClientFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        

        
    }//GEN-LAST:event_okBtnMouseClicked

    //Bouton annuler: revenir sur nos pas
    private void annulerBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_annulerBtnMouseClicked
        this.setVisible(false);
        CranberryFrame revenir = new CranberryFrame();
    }//GEN-LAST:event_annulerBtnMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton annulerBtn;
    private javax.swing.JLabel exMdp;
    private javax.swing.JLabel exName;
    private javax.swing.JTextField mdp;
    private javax.swing.JLabel mdpLbl;
    private javax.swing.JLabel nameLbl;
    private javax.swing.JButton okBtn;
    private javax.swing.JTextField utilisateur;
    // End of variables declaration//GEN-END:variables

}
