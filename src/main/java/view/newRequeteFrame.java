package view;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

import model.Request;
import model.User;
import service.RequestManager;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * newRequeteFrame.java
 *
 */
/**
 *
 * @author Anonyme
 */
public class newRequeteFrame extends javax.swing.JFrame {

    User someone;
    JFrame prec;
    JFileChooser choosing;
    String path;
    File file;

    /** Creates new form newRequeteFrame */
    public newRequeteFrame(User lui, JFrame pagePrecedente) {
        initComponents();
        this.setVisible(true);
        someone = lui;
        prec = pagePrecedente;

    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fileChooser = new javax.swing.JFileChooser();
        titleLbl = new javax.swing.JLabel();
        sujetLbl = new javax.swing.JLabel();
        sujetFld = new javax.swing.JTextField();
        descripLbl = new javax.swing.JLabel();
        descpScroll = new javax.swing.JScrollPane();
        descpArea = new javax.swing.JTextArea();
        fichierLbl = new javax.swing.JLabel();
        pathFld = new javax.swing.JTextField();
        selPathBtn = new javax.swing.JButton();
        catBox = new javax.swing.JComboBox();
        catLbl = new javax.swing.JLabel();
        doneBtn = new javax.swing.JButton();
        quitBtn = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();

        fileChooser.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                fileChooserFocusGained(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        titleLbl.setText("Nouvelle requête");

        sujetLbl.setText("Sujet de la requête:");

        sujetFld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sujetFldActionPerformed(evt);
            }
        });

        descripLbl.setText("Description:");

        descpArea.setColumns(20);
        descpArea.setRows(5);
        descpArea.setBorder(null);
        descpScroll.setViewportView(descpArea);

        fichierLbl.setText("Fichier:");

        pathFld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pathFldActionPerformed(evt);
            }
        });

        selPathBtn.setText("Téléverser un fichier");
        selPathBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selPathBtnActionPerformed(evt);
            }
        });

        catBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Poste de travail", "Serveur", "Service web", "Compte usager", "Autre" }));
        catBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                catBoxActionPerformed(evt);
            }
        });

        catLbl.setText("Catégorie:");

        doneBtn.setText("Terminer");
        doneBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                doneBtnActionPerformed(evt);
            }
        });

        quitBtn.setText("Revenir");
        quitBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(titleLbl)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(sujetLbl)
                                    .addComponent(descripLbl)
                                    .addComponent(fichierLbl))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(pathFld, javax.swing.GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE)
                                    .addComponent(sujetFld, javax.swing.GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE)
                                    .addComponent(descpScroll, javax.swing.GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE)
                                    .addComponent(selPathBtn))))
                        .addContainerGap(30, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(catLbl)
                        .addGap(63, 63, 63)
                        .addComponent(catBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(176, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(124, 124, 124)
                        .addComponent(doneBtn)
                        .addGap(18, 18, 18)
                        .addComponent(quitBtn)
                        .addContainerGap(104, Short.MAX_VALUE))))
            .addComponent(jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titleLbl)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sujetLbl)
                    .addComponent(sujetFld, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(descripLbl)
                    .addComponent(descpScroll, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pathFld, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fichierLbl))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(selPathBtn)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(catBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(catLbl))
                .addGap(27, 27, 27)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(quitBtn)
                    .addComponent(doneBtn))
                .addContainerGap(31, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void sujetFldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sujetFldActionPerformed
        // TODO add your handling code here:
        //a supprimer
    }//GEN-LAST:event_sujetFldActionPerformed

    //affiche le path du fichier
    private void pathFldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pathFldActionPerformed
        pathFld.setText(fileChooser.getSelectedFile().getPath());
}//GEN-LAST:event_pathFldActionPerformed

    //FileShooser: on crée un nouveau fichier dans le /dat pour le conserver
    //peut importe d'où il vient pour que le technicien puisse l'ouvrir
    private void selPathBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selPathBtnActionPerformed
        fileChooser.showOpenDialog(prec);

        pathFldActionPerformed(evt);
        try {
            file = new File("dat/" + fileChooser.getSelectedFile().getName());
            InputStream srcFile = new FileInputStream(fileChooser.getSelectedFile());
            OutputStream newFile = new FileOutputStream(file);
            byte[] buf = new byte[4096];
            int len;
            while ((len = srcFile.read(buf)) > 0) {
                newFile.write(buf, 0, len);
            }
            srcFile.close();
            newFile.close();
        } catch (IOException ex) {
            Logger.getLogger(newRequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_selPathBtnActionPerformed

    private void catBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_catBoxActionPerformed
    }//GEN-LAST:event_catBoxActionPerformed

    //Crée la requête avec toutes les informations entrée dans les cases
    //et les objets sélectionnés dans les menus
    private void doneBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_doneBtnActionPerformed
        try {
            RequestManager.getInstance().addRequest(sujetFld.getText(), descpArea.getText().replaceAll("\n", " "),
                    someone, Request.Category.fromString((String) catBox.getSelectedItem()));

            Request tempo = RequestManager.getInstance().getLastRequest();
            RequestManager.getInstance().getLastRequest().setFile(file);

            this.setVisible(false);
            if (someone.getRole().equals("client")) {
                ClientLoggedFrame retourPageClient = new ClientLoggedFrame(someone);
                retourPageClient.setVisible(true);
            } else {
                TechLoggedFrame retourPageTech = new TechLoggedFrame(someone);
                retourPageTech.setVisible(true);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(newRequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(newRequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
        }



    }//GEN-LAST:event_doneBtnActionPerformed

    //Quitter: revenir à la fenêtre précédante
    private void quitBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quitBtnActionPerformed
        this.setVisible(false);
        prec.setVisible(true);

    }//GEN-LAST:event_quitBtnActionPerformed

    private void fileChooserFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_fileChooserFocusGained
        //a supprimer
    }//GEN-LAST:event_fileChooserFocusGained
    /**
     * @param args the command line arguments
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox catBox;
    private javax.swing.JLabel catLbl;
    private javax.swing.JTextArea descpArea;
    private javax.swing.JScrollPane descpScroll;
    private javax.swing.JLabel descripLbl;
    private javax.swing.JButton doneBtn;
    private javax.swing.JLabel fichierLbl;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField pathFld;
    private javax.swing.JButton quitBtn;
    private javax.swing.JButton selPathBtn;
    private javax.swing.JTextField sujetFld;
    private javax.swing.JLabel sujetLbl;
    private javax.swing.JLabel titleLbl;
    // End of variables declaration//GEN-END:variables
}
