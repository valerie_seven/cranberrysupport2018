import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import model.Comment;
import model.Request;
import model.Technician;

@RunWith(MockitoJUnitRunner.class)
class RequestTest {
	
	Request request;

	@Mock
	Technician user;
	@Mock
	Comment comment;
	
	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		request = new Request("Sujet", "Description", user, Request.Category.OTHER);
	}

	@AfterEach
	void tearDown() throws Exception {
		request = null;
	}

	@Test
	void testAddComment() {
		request.addComment(comment);
		assertEquals(1, request.getComments().size());
	}

}
