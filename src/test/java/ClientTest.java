import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import model.Client;
import model.Request;

@RunWith(MockitoJUnitRunner.class)
class ClientTest {
	private final String USER_FIRSTNAME = "fisrtName";
	private final String USER_LASTNAME = "lastName";
	private final String USER_USERNAME = "username";
	private final String USER_PASSWORD = "password";
	private final String USER_ROLE = "role";

	Client client;

	@Mock
	Request request;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		client = new Client(USER_FIRSTNAME, USER_LASTNAME, USER_USERNAME, USER_PASSWORD, USER_ROLE);
	}

	@AfterEach
	void tearDown() throws Exception {
		client = null;
	}

	@Test
	void testFindRequest() {
		client.findRequest();
	}
}