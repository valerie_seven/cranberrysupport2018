import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import model.Comment;
import model.User;
import util.Character;

@RunWith(MockitoJUnitRunner.class)
class CommentTest {
	private final String COMMENT = "comment";
	
	Comment comment;

	@Mock
	User user;
	
	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		comment = new Comment(COMMENT, user);
	}

	@AfterEach
	void tearDown() throws Exception {
		comment = null;
	}

	@Test
	void testToString() {
		String commentToString = comment.getAuthor().getUsername() + Character.COLON + comment.getComment() + Character.NEW_LINE;
		assertEquals(commentToString, comment.toString());
	}

}
