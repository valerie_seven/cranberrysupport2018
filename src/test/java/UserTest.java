import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import model.Client;
import model.Request;
import model.User;

@RunWith(MockitoJUnitRunner.class)
class UserTest {
	private final String USER_FIRSTNAME = "fisrtName";
	private final String USER_LASTNAME = "lastName";
	private final String USER_USERNAME = "username";
	private final String USER_PASSWORD = "password";
	private final String USER_ROLE = "role";
	
	User user;
	
	@Mock
	Request request;
	
	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		user = new Client(USER_FIRSTNAME, USER_LASTNAME, USER_USERNAME, USER_PASSWORD, USER_ROLE);
	}

	@AfterEach
	void tearDown() throws Exception {
		user = null;
	}

	@Test
	void testGetInformations() {
		ArrayList<String> infos = new ArrayList<>();
		
		infos.add(user.getLastname());
		infos.add(user.getFirstname());
		infos.add(user.getUsername());
		infos.add(user.getRole());
		
		ArrayList<String> actualInfos = user.getInformations();
		
		for (int i=0; i<infos.size(); i++) {
			assertEquals(infos.get(i), actualInfos.get(i));
		}	
	}
	
	@Test
	void testIsCredentialValid() {
		assertTrue(user.isCredentialValid(USER_USERNAME, USER_PASSWORD));
	}
	
	@Test
	void testIsCredentialValidDifferentCase() {
		assertTrue(user.isCredentialValid(USER_USERNAME.toUpperCase(), USER_PASSWORD));
	}
	
	@Test
	void testIsCredentialValidBadUsername() {
		assertFalse(user.isCredentialValid("bad Username", USER_PASSWORD));
	}
	
	@Test
	void testIsCredentialValidBadPassword() {
		assertFalse(user.isCredentialValid(USER_USERNAME, "bad password"));
	}

	@Test
	void testAddRequest() {
		user.addRequest(request);
		assertEquals(1, user.getRequests().size());
	}
	
	@Test
	void testGetRequestByStatut() {
		Mockito.when(request.getStatut()).thenReturn(Request.Statut.CURRENT);
		user.addRequest(request);
		assertEquals(1, user.getRequests(Request.Statut.CURRENT).size());
	}

	@Test
	void testBadGetRequestByStatut() {
		Mockito.when(request.getStatut()).thenReturn(Request.Statut.CURRENT);
		user.addRequest(request);
		assertEquals(0, user.getRequests(Request.Statut.OPEN).size());
	}

}
