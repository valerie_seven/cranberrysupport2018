import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import model.Request;
import model.Technician;

@RunWith(MockitoJUnitRunner.class)
class TechnicianTest {
	private final String TECHNICIEN_FIRSTNAME = "fisrtName";
	private final String TECHNICIEN_LASTNAME = "lastName";
	private final String TECHNICIEN_USERNAME = "username";
	private final String TECHNICIEN_PASSWORD = "password";
	private final String TECHNICIEN_ROLE = "role";

	Technician technician;

	@Mock 
	Request request;
	@Mock 
	Request requestOpen;
	@Mock 
	Request requestSuccess;
	@Mock 
	Request requestAbandoned;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		technician = new Technician(TECHNICIEN_FIRSTNAME, TECHNICIEN_LASTNAME, TECHNICIEN_USERNAME, TECHNICIEN_PASSWORD,
		    TECHNICIEN_ROLE);
	}

	@AfterEach
	void tearDown() throws Exception {
		technician = null;
	}

	@Test
	void testGetPersonnalRequests() {
		Mockito.when(request.getClient()).thenReturn(technician);
		technician.addRequest(request);
		assertEquals(1, technician.getPersonnalRequests().size());
	}

	@Test
	void testGetNumberOfRequestByStatut() {
		Mockito.when(request.getStatut()).thenReturn(Request.Statut.CURRENT);
		Mockito.when(requestOpen.getStatut()).thenReturn(Request.Statut.OPEN);
		Mockito.when(requestSuccess.getStatut()).thenReturn(Request.Statut.SUCCESS);
		Mockito.when(requestAbandoned.getStatut()).thenReturn(Request.Statut.ABANDON);

		technician.addRequest(request);
		technician.addRequest(requestOpen);
		technician.addRequest(requestSuccess);
		technician.addRequest(requestAbandoned);

		String result = "";

		for (Request.Statut statut : Request.Statut.values()) {
			result += Technician.STATUT + statut.toString() + ": " + 1 + "\n";
		}

		assertEquals(result, technician.getNumberOfRequestPerStatut());
	}

}
