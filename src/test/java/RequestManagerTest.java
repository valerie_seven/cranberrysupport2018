import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import model.Client;
import model.Request;
import model.Technician;
import service.RequestManager;

@RunWith(MockitoJUnitRunner.class)
class RequestManagerTest {
	private final String TECHNICIEN_FIRSTNAME = "fisrtName";
	private final String TECHNICIEN_LASTNAME = "lastName";
	private final String TECHNICIEN_USERNAME = "username";
	private final String TECHNICIEN_PASSWORD = "password";
	private final String TECHNICIEN_ROLE = "role";
	RequestManager requestManager;

	@Mock
	Client user;

	@Mock
	Technician technician;
	
	@Mock
	Request request;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		requestManager = RequestManager.getInstance();
	}

	@AfterEach
	void tearDown() throws Exception {
		requestManager = null;
	}

	@Test
	void testAddRequest() throws FileNotFoundException, IOException {
		requestManager.getRequests().clear();
		requestManager.addRequest("sujet", "desrcrip", user, Request.Category.OTHER);
		assertEquals(1, requestManager.getRequests().size());
	}
	
	@Test
	void testGetRequestsByStatut() throws FileNotFoundException, IOException {
		requestManager.getRequests().clear();
		requestManager.addRequest("sujet", "desrcrip", user, Request.Category.OTHER);
		assertEquals(1, requestManager.getRequests(Request.Statut.OPEN).size());
	}
	
	@Test
	void testGetLastRequest() throws FileNotFoundException, IOException {
		requestManager.getRequests().clear();
		requestManager.addRequest("sujet", "desrcrip", user, Request.Category.OTHER);
		Request lastRequest = requestManager.getLastRequest();
		assertEquals("sujet", lastRequest.getSubject());
	}
	
	@Test
	void testAssignTechnicianToRequest(){
		Mockito.when(request.getTechnician()).thenReturn(technician);
		requestManager.assignTechnicianToRequest(technician, request);
		assertEquals(technician, request.getTechnician());
	}
	
	@Test
	void TestSaveRequest() throws IOException {
		requestManager.saveRequests();
		File file = new File(requestManager.DAT_BANQUE_REQUETES_TXT);
		assertTrue(file.exists());
	}

}
